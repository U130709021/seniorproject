package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

public class AyarlaBasla extends AppCompatActivity {
    ArrayList dataModels;
    ListView listView;
    private CustomAdapter adapter;
    ImageButton sonraki_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayarla_basla);

        ImageButton sonraki_1 = (ImageButton) findViewById(R.id.sonraki1);
        sonraki_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),eylem_1.class);
                startActivity(i);

            }
        });



        listView = (ListView) findViewById(R.id.listview);

        dataModels = new ArrayList();

        dataModels.add(new DataModel("uyudu", false));
        dataModels.add(new DataModel("su içti", false));
        dataModels.add(new DataModel("karpuz yedi", false));
        dataModels.add(new DataModel("yüzdü", false));
        dataModels.add(new DataModel("televizyon izledi", false));
        dataModels.add(new DataModel("kitap okudu", false));
        dataModels.add(new DataModel("okula gitti", false));





        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                DataModel dataModel= (DataModel) dataModels.get(position);
                dataModel.checked = !dataModel.checked;
                adapter.notifyDataSetChanged();


            }
        });
    }
}
