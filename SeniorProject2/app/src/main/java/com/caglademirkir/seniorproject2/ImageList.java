package com.caglademirkir.seniorproject2;

import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

public class ImageList extends AppCompatActivity {
    final VeriTabani veriTabani = new VeriTabani(ImageList.this);

    GridView gridView;
    ArrayList<Image> list;
    ImageListAdapter adapter = null;

    public void listImages(){
        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<>();
        adapter = new ImageListAdapter(this, R.layout.image_items, list);
        gridView.setAdapter(adapter);

        //get all data from sqlite
        Cursor cursor = veriTabani.getData("SELECT * FROM NE_YAPIYOR");
        list.clear();
        System.out.println("Ersan" + cursor.getCount());
        while(cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String title = cursor.getString(1);
            byte[] image = cursor.getBlob(2);

            list.add(new Image(id, title, image));
        }

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("Ersan");
        setContentView(R.layout.image_list_activity);
        this.listImages();




        gridView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                if (v.getId() == R.id.gridView) {
                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                    Image selectedItem = (Image)gridView.getItemAtPosition(info.position);
                    menu.setHeaderTitle(selectedItem.getName());
                    menu.add(0, 0, 0, "Sil");
                }

            }
        });

    }

    public boolean onContextItemSelected(MenuItem item){

        boolean donus;
        switch (item.getItemId()){
            case 0:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
                final Image secili = (Image) gridView.getItemAtPosition(info.position);
                System.out.println("ersan 92" + secili.getId() + " " + secili.getName());
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(" Veri Silme ");
                builder.setMessage(" \" " + secili.getName() + " \" adlı veriyi silmek istediğinize emin misiniz ? ");
                builder.setNegativeButton(" Evet ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        veriTabani.veriSilResim(secili.getId());
                        listImages();
                    }
                });
                builder.setPositiveButton(" Hayır ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                donus = true;
                break;
            default:
                donus = false;
                break;
        }
        return donus;

    }
}
