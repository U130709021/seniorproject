package com.caglademirkir.seniorproject2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class Oyun1_1 extends AppCompatActivity {
    ImageButton eslestir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun1_1);
        @SuppressLint("WrongViewCast") ImageButton eslestir = (ImageButton) findViewById(R.id.eslestir1);
        eslestir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun1_2.class);
                startActivity(i);

            }
        });
    }
}
