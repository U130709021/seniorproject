package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Oyun2_3 extends AppCompatActivity {

    Button cagla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun2_3);
        Button cagla = (Button) findViewById(R.id.cagla);
        cagla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(getApplication(),Oyun2_4.class);
                startActivity(i);
            }
        });
    }
}
