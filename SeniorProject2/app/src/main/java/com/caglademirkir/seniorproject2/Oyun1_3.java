package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Oyun1_3 extends AppCompatActivity {
    Button nazan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun1_3);
        Button nazan = (Button) findViewById(R.id.mutfakta1);
        nazan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun1_4.class);
                startActivity(i);

            }
        });
    }
}
