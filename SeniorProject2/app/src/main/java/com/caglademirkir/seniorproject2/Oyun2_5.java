package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Oyun2_5 extends AppCompatActivity {
    Button mutfakta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun2_5);
        Button mutfakta = (Button) findViewById(R.id.mutfakta1);
        mutfakta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun2_6.class);
                startActivity(i);

            }
        });
    }
}
