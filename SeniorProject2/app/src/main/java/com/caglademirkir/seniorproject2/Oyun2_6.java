package com.caglademirkir.seniorproject2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class Oyun2_6 extends AppCompatActivity {
    ImageButton dev;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun2_6);
        @SuppressLint("WrongViewCast") ImageButton dev = (ImageButton) findViewById(R.id.devamm);
        dev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun_son.class);
                startActivity(i);
            }
        });
    }
}
