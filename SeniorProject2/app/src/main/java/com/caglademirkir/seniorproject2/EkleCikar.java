package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class EkleCikar extends AppCompatActivity {

    ImageButton neyapiyor;
    ImageButton nerede;
    ImageButton nezaman;
    ImageButton anasayfa;
    ImageButton kimkim;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekle_cikar);

        ImageButton neyapiyor = (ImageButton) findViewById(R.id.neyapiyorr);
        neyapiyor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Neyapiyor_yonetici.class);
                startActivity(i);
            }
        });

        ImageButton nerede  = (ImageButton) findViewById(R.id.neredebtn);
        nerede.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(getApplication(),Nerede_yonetici.class);
                startActivity(a);
            }
        });

        ImageButton nezaman = (ImageButton) findViewById(R.id.nezamanbtn);

        nezaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),Nezaman_yonetici.class);
                startActivity(i);
            }
        });


        ImageButton anasayfa = (ImageButton) findViewById(R.id.anasayfa);
        anasayfa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent anasayfa = new Intent(getApplication(),MainActivity.class);
                startActivity(anasayfa);
            }
        });

        ImageButton kimkim = (ImageButton) findViewById(R.id.kimkim);
        kimkim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kimkim = new Intent(getApplication(),Kim_yonetici.class);
                startActivity(kimkim);
            }
        });






    }


}
