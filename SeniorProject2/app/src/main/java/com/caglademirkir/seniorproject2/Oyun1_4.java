package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Oyun1_4 extends AppCompatActivity {
    Button dun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun1_4);
        Button dun = (Button) findViewById(R.id.dun1);
        dun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun1_5.class);
                startActivity(i);

            }
        });
    }
}
