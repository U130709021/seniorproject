package com.caglademirkir.seniorproject2;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.List;

public class Nerede_yonetici extends AppCompatActivity {

    private EditText etAd;
    private ImageButton btnKaydet;
    private ImageButton btnListele;
    private ListView listView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nerede_yonetici);


        etAd = (EditText) findViewById(R.id.etAd);
       ImageButton btnKaydet = (ImageButton) findViewById(R.id.btnKaydet);
        @SuppressLint("WrongViewCast") ImageButton btnListele = (ImageButton) findViewById(R.id.btnListele);
        listView = (ListView) findViewById(R.id.VeriListele);


        btnKaydet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Nerede_yonetici.this);
                veriTabani.VeriEkle(etAd.getText().toString());

            }
        });

        final VeriTabani veriTabani = new VeriTabani(Nerede_yonetici.this);
        final List<String> vVeriler = veriTabani.VeriListele();
        registerForContextMenu(listView);

        btnListele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Nerede_yonetici.this);
                List<String> vVeriler = veriTabani.VeriListele();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Nerede_yonetici.this, android.R.layout.simple_list_item_1, android.R.id.text1, vVeriler);
                listView.setAdapter(adapter);
            }
        });

        listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                if (v.getId() == R.id.VeriListele) {
                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                    menu.setHeaderTitle(listView.getItemAtPosition(info.position).toString());
                    menu.add(0, 0, 0, "Sil");
                }

            }
        });


    }

    public boolean onContextItemSelected(MenuItem item){

        boolean donus;
        switch (item.getItemId()){
            case 0:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
                final String secili = listView.getItemAtPosition(info.position).toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(" Veri Silme ");
                builder.setMessage(" \" " + secili + " \" adlı veriyi silmek istediğinize emin misiniz ? ");
                builder.setNegativeButton(" Evet ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String[] dizi = secili.split(" - ");
                        long id = Long.parseLong(dizi[0].trim());
                        VeriTabani veriTabani = new VeriTabani(Nerede_yonetici.this);
                        veriTabani.VeriSil(id);
                        veriTabani.VeriListele();
                    }
                });
                builder.setPositiveButton(" Hayır ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                donus = true;
                break;
                default:
                    donus = false;
                    break;
        }
        return donus;

    }

}
