package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class Ipucu1 extends AppCompatActivity {
    ImageButton ip1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipucu1);
        ImageButton ip1 = (ImageButton) findViewById(R.id.ipsu);
        ip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =  new Intent(getApplication(),Oyun_son.class);
                startActivity(i);
            }
        });

    }
}
