package com.caglademirkir.seniorproject2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.List;

public class Kim_yonetici extends AppCompatActivity {

    private EditText etAd3;
    private ImageButton btnKaydet3;
    private ImageButton btnListele3;
    private ListView listView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kim_yonetici);
        
        etAd3 = (EditText) findViewById(R.id.etAd3);
       ImageButton btnKaydet3 = (ImageButton) findViewById(R.id.btnKaydet3);
        ImageButton btnListele3 = (ImageButton) findViewById(R.id.btnListele3);
        listView3 = (ListView) findViewById(R.id.VeriListele3);

        btnKaydet3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Kim_yonetici.this);
                veriTabani.VeriEkleKim(etAd3.getText().toString());
            }
        });

        final VeriTabani veriTabani = new VeriTabani(Kim_yonetici.this);
        final List<String> vVeriler = veriTabani.VeriListeleKim();
        registerForContextMenu(listView3);

        btnListele3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Kim_yonetici.this);
                List<String> vVeriler = veriTabani.VeriListeleKim();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Kim_yonetici.this, android.R.layout.simple_list_item_1, android.R.id.text1, vVeriler);
                listView3.setAdapter(adapter);
            }
        });

        listView3.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                if (v.getId() == R.id.VeriListele3) {
                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                    menu.setHeaderTitle(listView3.getItemAtPosition(info.position).toString());
                    menu.add(0, 0, 0, "Sil");
                }

            }
        });


    }

    public boolean onContextItemSelected(MenuItem item){

        boolean donus;
        switch (item.getItemId()){
            case 0:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
                final String secili = listView3.getItemAtPosition(info.position).toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(" Veri Silme ");
                builder.setMessage(" \" " + secili + " \" adlı veriyi silmek istediğinize emin misiniz ? ");
                builder.setNegativeButton(" Evet ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String[] dizi = secili.split(" - ");
                        long id = Long.parseLong(dizi[0].trim());
                        VeriTabani veriTabani = new VeriTabani(Kim_yonetici.this);
                        veriTabani.VeriSilKim(id);
                        veriTabani.VeriListeleKim();
                    }
                });
                builder.setPositiveButton(" Hayır ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                donus = true;
                break;
            default:
                donus = false;
                break;
        }
        return donus;

    }

}
