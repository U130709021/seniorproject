package com.caglademirkir.seniorproject2;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.List;

public class Nezaman_yonetici extends AppCompatActivity {

    private EditText etAd2;
    private ImageButton btnKaydet2;
    private ImageButton btnListele2;
    private ListView listView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nezaman_yonetici);

        etAd2 = (EditText) findViewById(R.id.etAd2);
        ImageButton btnKaydet2 = (ImageButton) findViewById(R.id.btnKaydet2);
       @SuppressLint("WrongViewCast") ImageButton btnListele2 = (ImageButton) findViewById(R.id.btnListele2);
        listView2 = (ListView) findViewById(R.id.VeriListele2);

        btnKaydet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Nezaman_yonetici.this);
                veriTabani.VeriEkleNezaman(etAd2.getText().toString());
            }
        });

        final VeriTabani veriTabani = new VeriTabani(Nezaman_yonetici.this);
        final List<String> vVeriler = veriTabani.VeriListeleNezaman();
        registerForContextMenu(listView2);

        btnListele2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VeriTabani veriTabani = new VeriTabani(Nezaman_yonetici.this);
                List<String> vVeriler = veriTabani.VeriListeleNezaman();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Nezaman_yonetici.this, android.R.layout.simple_list_item_1, android.R.id.text1, vVeriler);
                listView2.setAdapter(adapter);
            }
        });

        listView2.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                if (v.getId() == R.id.VeriListele2) {
                    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                    menu.setHeaderTitle(listView2.getItemAtPosition(info.position).toString());
                    menu.add(0, 0, 0, "Sil");
                    //menu.add(0, 1, 0, "Düzenle");
                }

            }
        });

    }

    public boolean onContextItemSelected(MenuItem item){

        boolean donus;
        switch (item.getItemId()){
            case 0:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
                final String secili = listView2.getItemAtPosition(info.position).toString();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(" Veri Silme ");
                builder.setMessage(" \" " + secili + " \" adlı veriyi silmek istediğinize emin misiniz ? ");
                builder.setNegativeButton(" Evet ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String[] dizi = secili.split(" - ");
                        long id = Long.parseLong(dizi[0].trim());
                        VeriTabani veriTabani = new VeriTabani(Nezaman_yonetici.this);
                        veriTabani.VeriSilNezaman(id);
                        veriTabani.VeriListeleNezaman();
                    }
                });
                builder.setPositiveButton(" Hayır ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                donus = true;
                break;
            default:
                donus = false;
                break;
        }
        return donus;

    }
}
