package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Oyun2_2 extends AppCompatActivity {

    Button karpuz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun2_2);

        Button karpuz = (Button) findViewById(R.id.karpuz2);
        karpuz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun2_3.class);
                startActivity(i);
            }
        });
    }
}
