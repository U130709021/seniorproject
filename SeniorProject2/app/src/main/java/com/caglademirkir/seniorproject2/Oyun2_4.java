package com.caglademirkir.seniorproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class Oyun2_4 extends AppCompatActivity {
    Button aksam2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oyun2_4);
        Button aksam2 = (Button) findViewById(R.id.aksam);
        aksam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(),Oyun2_5.class);
                startActivity(i);
            }
        });
    }
}
