package com.caglademirkir.seniorproject2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cagla on 18/04/01.
 */

public class VeriTabani extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "veritabani";
    private static final int DATABASE_VERSION = 1;

    //Nerede Table Information
    private static final String NEREDE_TABLE = "nerede";

    public static final String ROW_ID = "id";
    public static final String ROW_NAME = "ad";

    //Nezaman Table Information
    private static final String NEZAMAN_TABLE = "nezaman";

    public static final String ROW_ID_NEZAMAN = "id_nezaman";
    public static final String ROW_NAME_NEZAMAN = "ad_nezaman";

    //Kim Table Information
    private static final String KIM_TABLE = "kim";

    public static final String ROW_ID_KIM = "id_kim";
    public static final String ROW_NAME_KIM = "ad_kim";




    public VeriTabani(Context context) {

        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + NEREDE_TABLE + " ( " + ROW_ID + " INTEGER PRIMARY KEY , " + ROW_NAME + " TEXT NOT NULL) ");

        db.execSQL(" CREATE TABLE " + NEZAMAN_TABLE + " ( " + ROW_ID_NEZAMAN + " INTEGER PRIMARY KEY , " + ROW_NAME_NEZAMAN + " TEXT NOT NULL) ");

        db.execSQL(" CREATE TABLE " + KIM_TABLE + " ( " + ROW_ID_KIM + " INTEGER PRIMARY KEY , " + ROW_NAME_KIM + " TEXT NOT NULL) ");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(" DROP TABLE IF EXISTS " + NEREDE_TABLE);

        db.execSQL(" DROP TABLE IF EXISTS " + NEZAMAN_TABLE);

        db.execSQL(" DROP TABLE IF EXISTS " + KIM_TABLE);

        onCreate(db);

    }

    public void VeriEkle(String ad){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROW_NAME, ad.trim());
        db.insert(NEREDE_TABLE, null, cv);
        db.close();

    }

    public List<String> VeriListele(){

        List<String> veriler = new ArrayList<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] sutunlar = {ROW_ID, ROW_NAME};
        Cursor cursor = db.query(NEREDE_TABLE, sutunlar,null, null, null, null, null );
        while (cursor.moveToNext()){
            veriler.add(cursor.getInt(0) + " - " + cursor.getString(1));
        }
        return veriler;
    }

    public void VeriSil(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NEREDE_TABLE, ROW_ID + " = " + id, null);
        db.close();
    }


    //NEZAMAN

    public void VeriEkleNezaman(String ad_nezaman){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROW_NAME_NEZAMAN, ad_nezaman.trim());
        db.insert(NEZAMAN_TABLE, null, cv);
        db.close();

    }

    public List<String> VeriListeleNezaman(){

        List<String> veriler2 = new ArrayList<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] sutunlar2 = {ROW_ID_NEZAMAN, ROW_NAME_NEZAMAN};
        Cursor cursor = db.query(NEZAMAN_TABLE, sutunlar2,null, null, null, null, null );
        while (cursor.moveToNext()){
            veriler2.add(cursor.getInt(0) + " - " + cursor.getString(1));
        }
        return veriler2;
    }

    public void VeriSilNezaman(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NEZAMAN_TABLE, ROW_ID_NEZAMAN + " = " + id, null);
        db.close();
    }


    //KIM

    public void VeriEkleKim(String ad_kim){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ROW_NAME_KIM, ad_kim.trim());
        db.insert(KIM_TABLE, null, cv);
        db.close();

    }

    public List<String> VeriListeleKim(){

        List<String> veriler3 = new ArrayList<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] sutunlar3 = {ROW_ID_KIM, ROW_NAME_KIM};
        Cursor cursor = db.query(KIM_TABLE, sutunlar3,null, null, null, null, null );
        while (cursor.moveToNext()){
            veriler3.add(cursor.getInt(0) + " - " + cursor.getString(1));
        }
        return veriler3;
    }


    public void VeriSilKim(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(KIM_TABLE, ROW_ID_KIM + " = " + id, null);
        db.close();
    }

    //Ne yapıyor/Resim

    public void queryData(String sql){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(sql);
    }

    public void insertData(String name, byte[] image){
        SQLiteDatabase db = getWritableDatabase();
        String sql = "INSERT INTO NE_YAPIYOR VALUES (NULL, ?,?)";

        SQLiteStatement statement = db.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1,name);
        statement.bindBlob(2,image);
        System.out.println("cagla" + image);
        statement.executeInsert();

    }

    public Cursor getData(String sql){
            SQLiteDatabase database = getReadableDatabase();
            return database.rawQuery(sql,null);
    }

    public void veriSilResim(long id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("NE_YAPIYOR", "Id" + " = " + id, null);
        db.close();
    }


}
